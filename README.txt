WHAT IT IS:

Twitter Follower is a Chrome extension to make multi-following (or unfollowing) of twitter accounts easier.

HOW IT WORKS:

It simply adds two buttons to twitter pages: "Follow them all!" and "Unfollow them!".

If you use it, the extension will simulate you're following/unfollowing all the users/accounts on the page: it will scroll to the first "Follow"/"Unfollow" button, click it, then wait a for a few and go for the next one... easy!

It works with the members of a list, search results and twitter suggestions. Well, it should work with any page where a "Follow"/"Unfollow" button appears. :)

Scrolling and delay give twitter some time to load more & more accounts at the bottom of the page.

Now, it also lets you to "Stop" the action you began.

LIMITATIONS:

Doesn't worry about twitter limits (per day, per account,...).

Doesn't work if an account you try to follow is suspended.

May stop working if twitter changes its layout.

CONTRIBUTE:

You can download the source code at https://bitbucket.org/marcis20/twitter-follower/ and modify it yourself.

CREDITS:

Logo from http://www.flickr.com/photos/jurgenappelo/7749081714/