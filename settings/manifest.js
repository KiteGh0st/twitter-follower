// SAMPLE
this.manifest = {
    "name": "Twitter Follower",
    "icon": "/img/icon48.png",
    "settings": [
        {
            "tab": i18n.get("Follow"),
            "group": i18n.get("Delay between Follow clicks (miliseconds)"),
            "name": "delay",
            "type": "text",
            "label": i18n.get("Not recommended to be set under 500")
        },
        {
            "tab": i18n.get("Follow"),
            "group": i18n.get("Use sidebar?"),
            "name": "sidebar",
            "type": "checkbox",
            "label": i18n.get("Follow accounts on sidebar?")
        },
        {
            "tab": i18n.get("Unfollow"),
            "group": i18n.get("Delay between Unfollow clicks (miliseconds)"),
            "name": "delayUnfollow",
            "type": "text",
            "label": i18n.get("Not recommended to be set under 500")
        }
    ]
};
