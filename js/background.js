var details = chrome.app.getDetails();

var settings = new Store("settings" + details.version, {
    "selectorFollow":    "button.follow-button .follow:visible, button.follow-button .follow-text:visible",
    "delay":             500,
    "selectorFollowing": "button.follow-button .following-text:visible",
    "selectorUnfollow":  ".unfollow-text",
    "delayUnfollow":     500,
    "selectorSidebar":   ".WhoToFollow, .wtf-module"
});

chrome.runtime.onMessage.addListener(
  function (request, sender, sendResponse) {
    console.log(request, sender);
    switch (request.action) {
        case 'getOptions':  sendResponse(settings.toObject());
                            break;
        default:            // Unknown action, do nothing
                            break;
    }
  }
);
