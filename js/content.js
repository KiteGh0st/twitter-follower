chrome.runtime.sendMessage({action: 'getOptions'}, function(theOptions) {
    console.log(theOptions);
    var stop;
    function followOne() {
        var $followButton;
        if (theOptions.sidebar) {
            $followButton = $(theOptions.selectorFollow).first();
        } else {
            $followButton = $(theOptions.selectorFollow).not($(theOptions.selectorSidebar).find(theOptions.selectorFollow)).first();
        }
        if (!stop && $followButton.length) {
            $followButton.ScrollTo({
                duration: parseInt(theOptions.delay),
    		    callback: followOne
            });
            $followButton.click();
        } else {
            stopFollowing();
        }
    }
    function unfollowOne() {
        var $followingButton = $(theOptions.selectorFollowing);
        if (!stop && $followingButton.length) {
            var $unfollowButton = $followingButton.parent().find(theOptions.selectorUnfollow).first();
            $followingButton.ScrollTo({
                duration: parseInt(theOptions.delayUnfollow),
    		    callback: unfollowOne
            });
            $unfollowButton.click();
        } else {
            stopUnfollowing();
        }
    }
    function followAll() {
        stop = false;
        $('#follow-them-all').html('STOP').unbind('click').click(stopFollowing);
        $('#unfollow-all').attr('disabled', true);
        followOne();
    }
    function unfollowAll() {
        stop = false;
        $('#unfollow-all').html('STOP').unbind('click').click(stopUnfollowing);
        $('#follow-them-all').attr('disabled', true);
        unfollowOne();        
    }
    function stopFollowing() {
        stop = true;
        $('#follow-them-all').html('Follow them all!').unbind('click').click(followAll);
        $('#unfollow-all').attr('disabled', false);
    }
    function stopUnfollowing() {
        stop = true;
        $('#unfollow-all').html('Unfollow all!').unbind('click').click(unfollowAll);
        $('#follow-them-all').attr('disabled', false);
    }

    $('body').append('<button id="follow-them-all" class="follow-button btn" type="button" style="position: fixed; top: 50px; right: 3px; z-index: 1000; padding: 6px;"></button>');
    stopFollowing();
    $('body').append('<button id="unfollow-all" class="follow-button btn" type="button" style="position: fixed; top: 83px; right: 3px; z-index: 1000; padding: 6px;"></button>');
    stopUnfollowing();
});