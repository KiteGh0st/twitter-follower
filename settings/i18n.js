this.i18n = {
    "settings": {
        "en": "Settings",
        "es": "Opciones"
    },
    "search": {
        "en": "Search",
        "es": "Buscar"
    },
    "nothing-found": {
        "en": "No matches were found.",
        "es": "No hay resultados."
    },

    "Follow": {
        "en": "Follow",
        "es": "Seguir"
    },
    "Unfollow": {
        "en": "Unfollow",
        "es": "Dejar de seguir"
    },
    
    "Follow button jQuery selector": {
        "en": "'Follow' button jQuery selector",
        "es": "Selector jQuery del botón 'Seguir'"
    },
    "Following button jQuery selector": {
        "en": "'Following' button jQuery selector",
        "es": "Selector jQuery del botón 'Siguiendo'"
    },
    "Unfollow button jQuery selector": {
        "en": "'Unfollow' button jQuery selector",
        "es": "Selector jQuery del botón 'Dejar de seguir'"
    },
    "If you don't know what it is, leave blank": {
        "en": "If you don't know what it is, don't modify!",
        "es": "Si no sabes lo que es, no lo modifiques!"
    },
    "Delay between Follow clicks (miliseconds)": {
        "en": "Delay between 'Follow' clicks (miliseconds)",
        "es": "Retardo entre clicks en 'Seguir' (milisegundos)"
    },
    "Delay between unfollow clicks (miliseconds)": {
        "en": "Delay between 'Unfollow' clicks (miliseconds)",
        "es": "Retardo entre clicks en 'Dejar de seguir' (milisegundos)"
    },
    "Not recommended to be set under 500": {
        "en": "Not recommended to be set under 500",
        "es": "Valor mínimo recomendado: 500"
    },
    "Use sidebar?": {
        "en": "Use sidebar?",
        "es": "¿Utilizar menú lateral?"
    },
    "Follow accounts on sidebar?": {
        "en": "Follow accounts on sidebar?",
        "es": "¿Seguir las cuentas del menú lateral?"
    }
};
